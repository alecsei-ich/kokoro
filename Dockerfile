FROM node:5.10.1

MAINTAINER alexey@lastbackend.com

ENV NPM_CONFIG_LOGLEVEL silly
ENV DEBUG *

EXPOSE 3000

ADD . /opt

RUN rm -rf /opt/node_modules
RUN cd /opt && npm install

# Setup main workdir
WORKDIR /opt
VOLUME /opt

# Default command
CMD node app.js
