'use strict';

const http    = require('http');
const express = require('express');

const port     = 3000;
var app        = express();

var main = require('./controllers/main');
var info = require('./controllers/info');
var cache = require('./controllers/cache');

app.get('/', main.load);

app.get('/info', info.load);

app.get('/cache', cache.query);
app.get('/cache/:uuid', cache.load);

app.listen(3000);
