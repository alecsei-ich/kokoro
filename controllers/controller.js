'use strict';

/**
 * @class Controller
 */
class Controller {
  query(req, res, name) {
    name = name || 'controller';
    console.log([name, 'query'].join(':'));
    return new Promise(resolve => resolve());
  }

  load(req, res, name) {
    name = name || 'controller';
    console.log([name, 'load', req.params.uuid].join(':'));
    var data  = req.body || {};
    data.uuid = req.params.uuid;

    return new Promise((resolve, reject) => data.uuid ? resolve(data) : reject());
  }

  create(req, res, name) {
    name = name || 'controller';
    console.log([name, 'create'].join(':'));
    var data  = req.body || {};
    data.uuid = req.params.uuid;

    return new Promise(resolve => resolve(data));
  }

  update(req, res, name) {
    name = name || 'controller';
    console.log([name, 'update', req.params.uuid].join(':'));
    var data  = req.body || {};
    data.uuid = req.params.uuid;

    return new Promise((resolve, reject) => data.uuid ? resolve(data) : reject());
  }

  remove(req, res, name) {
    name = name || 'controller';
    console.log([name, 'remove', req.params.uuid].join(':'));
    var uuid = req.params.uuid;

    return new Promise((resolve, reject) => uuid ? resolve(uuid) : reject());
  }
}

module.exports = Controller;
