'use strict';

/**
 * @class Main
 * @proto load
 */
class Main {

  /**
   * Load documentation
   * @param req
   * @param res
   * @returns {*}
   */
  load(req, res) {
    console.log('main:load');

    var data = {
      info: "Place for api doc"
    };

    return res.status(200).jsonp(data);
  }

}

module.exports = new Main();

