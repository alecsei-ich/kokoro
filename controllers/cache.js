'use strict';

const NAME     = 'cache';
var Controller = require('../controllers/controller');

/**
 * @class Cache
 */
class Cache extends Controller {
  query(req, res) {
    return super.query(req, res, NAME).then(result, error);

    function result() {
      return res.status(200).jsonp({list: []});
    }
  }

  load(req, res) {
    return super.load(req, res, NAME).then(result, error);

    function result(data) {
      return res.status(200).jsonp(data);
    }
  }

  create(req, res) {
    return super.create(req, res, NAME).then(result, error);

    function result(data) {
      return res.status(200).jsonp(data);
    }
  }

  update(req, res) {
    return super.update(req, res, NAME).then(result, error);

    function result(data) {
      return res.status(200).jsonp(data);
    }
  }

  remove(req, res) {
    return super.remove(req, res, NAME).then(result, error);

    function result(data) {
      return res.status(200).jsonp(data);
    }
  }

}

function error() {
  console.error('>> error > cache <');
}

module.exports = new Cache();

