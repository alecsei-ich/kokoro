'use strict';

var _package = require('../package.json');

/**
 * @class Info
 */
class Info {

  /**
   * Load information
   * @param req
   * @param res
   * @returns {*}
   */
  load(req, res) {
    console.log('info:load');

    var data = {
      version: _package.version
    };

    return res.status(200).jsonp(data);
  }

}

module.exports = new Info();

